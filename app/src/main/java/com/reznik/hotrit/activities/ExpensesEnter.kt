package com.reznik.hotrit.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife
import com.reznik.hotrit.App
import com.reznik.hotrit.R
import com.reznik.hotrit.models.dao.ArticleDao
import com.reznik.hotrit.models.dao.ExpensesDao
import com.reznik.hotrit.domen.Article
import com.reznik.hotrit.domen.Expenses
import com.reznik.hotrit.utils.*
import java.text.DateFormat
import java.util.*
import javax.inject.Inject

class ExpensesEnter : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var expensesDao: ExpensesDao

    @Inject
    lateinit var articleDao: ArticleDao

    @BindView(R.id.etAmount)
    lateinit var etAmountAEE: EditText

    @BindView(R.id.tvSelectArticle)
    lateinit var tvSelectArticle: TextView

    @BindView(R.id.tvSelectDate)
    lateinit var tvSelectDate: TextView

    private var expenses: Expenses? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expenses_enter)
        ButterKnife.bind(this)
        App.appComponent.injectExpensesEnter(this)
        etAmountAEE.limitedNumberInput()

        //редактирование/создание
        if (intent.getBooleanExtra(EDIT_EXPENSES, false)) {
            expenses = intent.getSerializableExtra(Expenses::class.simpleName) as Expenses?
            expenses?.dateCreate?.let {
                tvSelectDate.text = DateFormat.getDateInstance(DateFormat.MEDIUM).format(it)
            }
            expenses?.articleId?.let {
                tvSelectArticle.text = articleDao.getById(it).toString()
            }
            etAmountAEE.text.append(expenses?.amount.toString())
        } else {
            expenses = Expenses(dateCreate = Date())
            expenses?.dateCreate?.let {
                tvSelectDate.text = DateFormat.getDateInstance(DateFormat.MEDIUM).format(it)
            }
        }
    }

    //Выбор даты
    fun selectDate(view: View) {
        val c = Calendar.getInstance()
        expenses?.dateCreate?.let { c.time = it }
        val d = DatePickerDialog(
            this,
            android.R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth,
            this, c.get(Calendar.YEAR),
            c.get(Calendar.MONTH),
            c.get(Calendar.DAY_OF_MONTH)
        )
        d.show()
    }

    //Установка времени, после выбора
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val c = Calendar.getInstance()
        c.set(Calendar.YEAR, year)
        c.set(Calendar.MONTH, month)
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        expenses?.dateCreate = c.time
        tvSelectDate.text = DateFormat.getDateInstance(DateFormat.MEDIUM).format(c.time)
    }

    fun saveExpenses(view: View) {

        if (expenses?.validate(this) != true)
            return

        if (expenses?.id == 0L) {
            if (etAmountAEE.text?.length != VALUE_EMPTY)
                expenses?.amount = etAmountAEE.text?.toString()?.toDouble()
            expensesDao.insert(expenses!!)
        } else {
            if (etAmountAEE.text?.length != VALUE_EMPTY)
                expenses?.amount = etAmountAEE.text?.toString()?.toDouble()
            expensesDao.update(expenses!!)
        }
        finish()
    }

    fun selectArticle(view: View) {
        val intent = Intent(this, ArticleActivity::class.java)
        intent.putExtra(SELECT_ARTICLE, true)
        startActivityForResult(intent, SELECT_ARTICLE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    SELECT_ARTICLE_CODE -> {
                        val articleSelect = data?.extras?.getSerializable(Article::class.simpleName) as Article
                        expenses?.articleId = articleSelect.id
                        tvSelectArticle.text = articleSelect.toString()
                    }
                }
            }
            else -> {
                Toast.makeText(this, "Не выбран элемент!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
