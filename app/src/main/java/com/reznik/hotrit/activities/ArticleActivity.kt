package com.reznik.hotrit.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.reznik.hotrit.App
import com.reznik.hotrit.R
import com.reznik.hotrit.contracts.ArticleContractView
import com.reznik.hotrit.domen.Article
import com.reznik.hotrit.domen.ArticleAdapter
import com.reznik.hotrit.presenters.ArticlePresenter
import com.reznik.hotrit.utils.*
import javax.inject.Inject

class ArticleActivity : AppCompatActivity(), ArticleContractView {

    @Inject
    lateinit var presenter: ArticlePresenter

    @Inject
    lateinit var articleAdapter: ArticleAdapter

    @BindView(R.id.etSearchBarAA)
    lateinit var etSearchBar: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)
        ButterKnife.bind(this)
        App.appComponent.injectArticleActivity(this)

        init()
    }

    override fun onStart() {
        super.onStart()
        presenter.viewIsReady()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    //общее меню, инициализация
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_article, menu)
        return super.onCreateOptionsMenu(menu)
    }

    //общее меню, реализация
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_article_action_clear -> deleteAllArticles()
        }
        return super.onOptionsItemSelected(item)
    }

    fun updateSearch() {
        when (etSearchBar.text?.length) {
            VALUE_EMPTY -> presenter.searchEmpty()
            else -> presenter.searchArticles()
        }
    }

    fun addArticle(view: View) {
        presenter.add()
    }

    override fun enterArticleData() {
        startActivity(Intent(this, ArticleEnter::class.java))
    }

    fun deleteAllArticles() {
        presenter.clear()
    }

    override fun getSearchData() = etSearchBar.text.toString()

    override fun showArticles(articles: List<Article>) {
        articleAdapter.setData(articles)
    }

    override fun showToast(resId: Int) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show()
    }

    private fun init() {
        presenter.view = this
        presenter.attachView(this)

        etSearchBar.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = updateSearch()
        })

        articleAdapter = ArticleAdapter(this).apply {
            onClickItem = object : OnClickItem {
                override fun onClick(valueObj: Any) {
                    if (intent.getBooleanExtra(SELECT_ARTICLE, false)) {
                        setResult(RESULT_OK, Intent().apply { putExtra(Article::class.simpleName, valueObj as Article) })
                        finish()
                    }
                }
            }
            onContextMenuItem = object : OnContextMenuItem {
                override fun onClickItem(valueObj: Any, command: Int) {
                    when (command) {
                        CONTEXT_MENU_ITEM_EDIT -> {
                            startActivity(Intent(context, ArticleEnter::class.java).apply {
                                putExtra(EDIT_ARTICLE,true)
                                putExtra(Article::class.simpleName, valueObj as Article)
                            })
                        }
                        CONTEXT_MENU_ITEM_DELETE -> {
                            presenter.delete(valueObj)
                            Toast.makeText(context, "Удалён $valueObj", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

        findViewById<RecyclerView>(R.id.listArticles).apply {
            layoutManager = LinearLayoutManager(context).apply { orientation = LinearLayoutManager.VERTICAL }
            adapter = articleAdapter
        }
    }
}
