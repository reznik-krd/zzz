package com.reznik.hotrit.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.reznik.hotrit.App
import com.reznik.hotrit.R
import com.reznik.hotrit.service.ExpensesService
import java.util.*
import javax.inject.Inject

class MainActivity : Activity() {

    @Inject
    lateinit var expensesService: ExpensesService

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.appComponent.injectMainActivity(this)

        val bottomNavigationView = findViewById<View>(R.id.bottom_navigation) as BottomNavigationView
        bottomNavigationView.linksActivity(R.id.mainActivityMA, this)

        val date = Calendar.getInstance()

        val amountNow = findViewById<TextView>(R.id.textView6)
        amountNow.text = "${expensesService.calculateAmountExpensesForDate(date)} руб."

        date.add(Calendar.DATE, -1)
        val amountYesterday = findViewById<TextView>(R.id.textView8)
        amountYesterday.text = "${expensesService.calculateAmountExpensesForDate(date)} руб."
    }

    //Ничего не делаем, т.к. используем BottomNavigationView
    override fun onBackPressed() {}
}
