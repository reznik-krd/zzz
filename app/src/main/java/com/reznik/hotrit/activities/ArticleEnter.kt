package com.reznik.hotrit.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.reznik.hotrit.App
import com.reznik.hotrit.R
import com.reznik.hotrit.models.dao.ArticleDao
import com.reznik.hotrit.domen.Article
import com.reznik.hotrit.utils.EDIT_ARTICLE
import com.reznik.hotrit.utils.VALUE_EMPTY
import javax.inject.Inject

class ArticleEnter : AppCompatActivity() {

    @Inject
    lateinit var articleDao: ArticleDao

    @BindView(R.id.etNameAAE)
    lateinit var etNameAAE: EditText

    private var article: Article? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_enter)
        ButterKnife.bind(this)
        App.appComponent.injectArticleEnter(this)

        //редактирование
        if (intent.getBooleanExtra(EDIT_ARTICLE, false)) {
            article = intent.getSerializableExtra(Article::class.simpleName) as Article?
            etNameAAE.text.append(article?.name)
        }
    }

    fun addArticle(view: View) {
        when (etNameAAE.text?.length) {
            VALUE_EMPTY -> Toast.makeText(this, R.string.empty_values, Toast.LENGTH_SHORT).show()
            else -> {
                article?.let {
                    it.name = etNameAAE.text?.toString()
                    articleDao.update(it)
                } ?: run {
                    articleDao.insert(Article(name = etNameAAE.text?.toString()))
                }
                finish()
            }
        }
    }
}
