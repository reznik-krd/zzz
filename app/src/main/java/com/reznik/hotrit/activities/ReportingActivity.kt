package com.reznik.hotrit.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.reznik.hotrit.App
import com.reznik.hotrit.R
import com.reznik.hotrit.models.dao.ArticleDao
import com.reznik.hotrit.models.dao.ExpensesDao
import com.reznik.hotrit.domen.Expenses
import com.reznik.hotrit.utils.DateUtilsRN
import java.text.DateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ReportingActivity : Activity(), DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var expensesDao: ExpensesDao

    @Inject
    lateinit var  articleDao: ArticleDao

    @BindView(R.id.piechart)
    lateinit var pieChart: PieChart

    @BindView(R.id.tvSelectDateFrom)
    lateinit var tvSelectDateFrom: TextView

    @BindView(R.id.tvSelectDateTo)
    lateinit var tvSelectDateTo: TextView

    private var expenses: MutableList<Expenses>? = null
    private var dateFrom: Date = Date(DateUtilsRN.getStartOfDayInMillis())
    private var dateTo: Date = Date(DateUtilsRN.getEndOfDayInMillis())
    private var fromTo: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reporting)
        ButterKnife.bind(this)
        App.appComponent.injectReportingActivity(this)

        init()
        downloadData()
        drawReporting()
    }

    //Ничего не делаем, т.к. используем BottomNavigationView
    override fun onBackPressed() {}

    fun init() {
        val bottomNavigationView = findViewById<View>(R.id.bottom_navigation) as BottomNavigationView
        bottomNavigationView.linksActivity(R.id.reportingActivityMA, this)

        //Настройки фильтров, по умолчанию - текущий месяц
        val currentDate = Calendar.getInstance()
        currentDate.set(Calendar.DAY_OF_MONTH, 1)
        dateFrom = Date(DateUtilsRN.getStartOfDayInMillis(currentDate.timeInMillis))

        tvSelectDateFrom.text = DateFormat.getDateInstance(DateFormat.MEDIUM).format(dateFrom)
        tvSelectDateTo.text = DateFormat.getDateInstance(DateFormat.MEDIUM).format(dateTo)
    }

    fun downloadData() {
        expenses = expensesDao.getAllForPeriod(dateFrom, dateTo) as MutableList<Expenses>?
    }

    fun selectDateFrom(view: View) {
        fromTo = false
        val c = Calendar.getInstance()
        c.time = dateFrom
        val d = DatePickerDialog(
            this,
            android.R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth,
            this, c.get(Calendar.YEAR),
            c.get(Calendar.MONTH),
            c.get(Calendar.DAY_OF_MONTH))
        d.show()
    }

    fun selectDateTo(view: View) {
        fromTo = true
        val c = Calendar.getInstance()
        c.time = dateTo
        val d = DatePickerDialog(
            this,
            android.R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth,
            this, c.get(Calendar.YEAR),
            c.get(Calendar.MONTH),
            c.get(Calendar.DAY_OF_MONTH))
        d.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val c = Calendar.getInstance()
        c.set(Calendar.YEAR, year)
        c.set(Calendar.MONTH, month)
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        when (fromTo) {
            false -> {
                dateFrom = Date(DateUtilsRN.getStartOfDayInMillis(c.timeInMillis))
                tvSelectDateFrom.text = DateFormat.getDateInstance(DateFormat.MEDIUM).format(dateFrom)
            }
            true -> {
                dateTo = Date(DateUtilsRN.getEndOfDayInMillis(c.timeInMillis))
                tvSelectDateTo.text = DateFormat.getDateInstance(DateFormat.MEDIUM).format(dateTo)
            }
        }
        downloadData()
        drawReporting()
    }

    fun drawReporting() {

        //оформление
        pieChart.setUsePercentValues(false) //использовать проценты или значения
        pieChart.description?.isEnabled = false
        pieChart.isDrawHoleEnabled = true //внутри круг расширяет
        pieChart.transparentCircleRadius = 40f //внутренний круг
        pieChart.holeRadius = 30f //внутренний круг, его ширина
        pieChart.animateY(1000, Easing.EaseInOutCubic) //анимация при выводе пирога
        pieChart.setExtraOffsets(25f, 0f, 25f, 0f); //сужает круг (5f, 10f, 5f, 5f)
        pieChart.setHoleColor(Color.WHITE) //внутри круга цвет меняет
        //pieChart.highlightValues(null) //выделить ценности
        pieChart.dragDecelerationFrictionCoef = 0.95f //отвечает за вращение после тычка
        pieChart.isRotationEnabled = true //отвечает за вращение

        //Легенда (размещение)
        val legendPie = pieChart.legend
        legendPie.isEnabled = false
//        legendPie.verticalAlignment = Legend.LegendVerticalAlignment.TOP
//        legendPie.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
//        legendPie.orientation = Legend.LegendOrientation.VERTICAL
//        legendPie.textSize = 15f
//        legendPie.setDrawInside(false)
//        legendPie.isWordWrapEnabled = true //включить перенос слов

        val groupExpenses = expenses?.groupBy { it.toKeyGroup() }
        val sumObject = object {
            var sum = 0.0;
        }

        //заполнение
        val valuesPie = ArrayList<PieEntry>()
        groupExpenses?.map { group ->
            sumObject.sum = 0.0
            group.value.map { exp ->
                exp.amount?.let {
                    sumObject.sum += it
                }
            }
            val article = articleDao.getById(group.key.articleId!!)
            valuesPie.add(PieEntry(sumObject.sum.toFloat(), article?.name))
        }

        val dataSetPie = PieDataSet(valuesPie, "Расходы")
        dataSetPie.sliceSpace = 3f //долщина разделов пирога
        dataSetPie.selectionShift = 5f
        dataSetPie.colors = ColorTemplate.COLORFUL_COLORS.toList()

        pieDatasetSlice(dataSetPie)
        val dataPie = PieData(dataSetPie)

        dataPie.setValueTextSize(13f)
        dataPie.setValueTextColor(Color.BLACK)

        //установка данных
        pieChart.data = dataPie
    }

    private fun pieDatasetSlice(dataSet: PieDataSet): PieDataSet {
        //dataSet.xValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE //метка
        dataSet.yValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE //значение метки
        dataSet.valueLinePart1OffsetPercentage = 90f //указатель строчки по блоку
        /** When valuePosition is OutsideSlice, indicates offset as percentage out of the slice size  */
        dataSet.valueLinePart1Length = 0.6f
        /** When valuePosition is OutsideSlice, indicates length of first half of the line  */
        dataSet.valueLinePart2Length = 0.6f
        /** When valuePosition is OutsideSlice, indicates length of second half of the line  */
        dataSet.sliceSpace = 0.8f //расстояние между блоками пирога
        return dataSet
    }
}
