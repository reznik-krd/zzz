package com.reznik.hotrit

import android.app.Application
import com.reznik.hotrit.dagger2rn.*
import com.reznik.hotrit.dagger2rn.components.AppComponent
import com.reznik.hotrit.dagger2rn.components.DaggerAppComponent

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
            private set
    }

    /**
     * Для кодогенерации Dagger 2 -> build project
     */
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .storageModule(StorageModule())
            .presenterModule(PresenterModule())
            .adapterModule(AdapterModule())
            .serviceModule(ServiceModule())
            .build()
    }
}