package com.reznik.hotrit.utils

import java.util.*

class DateUtilsRN {

    companion object {
        fun getStartOfDayInMillis(): Long {
            val calendar: Calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            return calendar.timeInMillis
        }

        fun getStartOfDayInMillis(date: Long): Long {
            val calendar: Calendar = Calendar.getInstance()
            calendar.timeInMillis = date
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            return calendar.timeInMillis
        }

        fun getEndOfDayInMillis(): Long {
            return getStartOfDayInMillis() + 24 * 60 * 60 * 1000-1
        }

        fun getEndOfDayInMillis(date: Long): Long {
            return getStartOfDayInMillis(date) + 24 * 60 * 60 * 1000-1
        }
    }
}