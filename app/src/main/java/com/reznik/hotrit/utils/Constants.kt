package com.reznik.hotrit.utils

//Наименование базы данных
const val DATABASE_NAME = "hotrit_db"

//Пустое значение, для сравнения, чтобы не было магических чисел
const val VALUE_EMPTY = 0

//Контекстное меню, список команд
const val CONTEXT_MENU_ITEM_EDIT = 0
const val CONTEXT_MENU_ITEM_DELETE = 1

//Команды выборки данных
const val SELECT_ARTICLE = "SELECT_ARTICLE"
const val SELECT_ARTICLE_CODE = 56391

//Команды редактирования данных
const val EDIT_EXPENSES = "EDIT_EXPENSES"
const val EDIT_ARTICLE = "EDIT_ARTICLE"

//Контактная информация
const val OWNER_NAME = "Reznik Nikolay"
const val OWNER_MAIL = "reznikdeveloper@gmail.com"