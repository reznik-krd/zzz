package com.reznik.hotrit.contracts

import com.reznik.hotrit.domen.Article

interface ArticleContractView {
    fun enterArticleData()
    fun getSearchData(): String?
    fun showArticles(articles: List<Article>)
    fun showToast(resId: Int)
}