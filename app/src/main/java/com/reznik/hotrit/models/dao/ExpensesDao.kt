package com.reznik.hotrit.models.dao

import androidx.room.*
import com.reznik.hotrit.domen.Expenses
import java.util.*

@Dao
abstract class ExpensesDao {

    @Query("select * from expenses")
    abstract fun all(): List<Expenses>?

    @Query("select * from expenses where id = :id")
    abstract fun getById(id: Long): Expenses?

    @Query("""
        select *
        from expenses
        where date_create between :dateFrom and :dateTo""")
    abstract fun getAllForPeriod(dateFrom: Date, dateTo: Date): List<Expenses>?

    @Query("""
        select sum(amount)
        from expenses
        where date_create between :dateFrom and :dateTo""")
    abstract fun getAmountForPeriod(dateFrom: Date, dateTo: Date): Double

    fun insert(expenses: Expenses) {
        expenses.uuidRel = expenses.uuidRel ?: UUID.randomUUID()
        insertDao(expenses)
    }

    @Insert
    abstract fun insertDao(expenses: Expenses)

    @Update
    abstract fun update(expenses: Expenses)

    @Delete
    abstract fun delete(expenses: Expenses)

    @Query("DELETE FROM expenses")
    abstract fun deleteAll()
}