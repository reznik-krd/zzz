package com.reznik.hotrit.models

import android.os.AsyncTask
import com.reznik.hotrit.domen.Article
import com.reznik.hotrit.models.dao.ArticleDao

class ArticleModel(val articleDao: ArticleDao) {

    fun searchArticles(search: String, callback: LoadArticlesCallback) { SearchArticlesTask(callback).execute(search) }

    fun loadArticles(callback: LoadArticlesCallback) { LoadArticlesTask(callback).execute() }

    fun addArticle(article: Article, callback: CompleteCallback) { AddArticleTask(callback).execute(article) }

    fun clearArticles(completeCallback: CompleteCallback) { ClearArticlesTask(completeCallback).execute() }

    //_______________________________________________________________________________________________________________________

    interface LoadArticlesCallback { fun onLoad(articles: List<Article>) }

    interface CompleteCallback { fun onComplete() }

    //_______________________________________________________________________________________________________________________

    inner class SearchArticlesTask(private val callback: LoadArticlesCallback) : AsyncTask<String, Void?, List<Article>>() {
        override fun doInBackground(vararg params: String) = articleDao.searchByName(params[0])
        override fun onPostExecute(articles: List<Article>) = callback.onLoad(articles)
    }

    inner class LoadArticlesTask(private val callback: LoadArticlesCallback) : AsyncTask<Void?, Void?, List<Article>>() {
        override fun doInBackground(vararg params: Void?) = articleDao.all()
        override fun onPostExecute(articles: List<Article>) = callback.onLoad(articles)
    }

    inner class AddArticleTask(private val callback: CompleteCallback) : AsyncTask<Article, Void?, Void?>() {
        override fun doInBackground(vararg params: Article): Void? {
            params[0].let { articleDao.insert(it) }
            return null
        }
        override fun onPostExecute(aVoid: Void?) = callback.onComplete()
    }

    inner class ClearArticlesTask(private val callback: CompleteCallback) : AsyncTask<Void?, Void?, Void?>() {
        override fun doInBackground(vararg params: Void?): Void? {
            articleDao.deleteAll()
            return null
        }
        override fun onPostExecute(aVoid: Void?) = callback.onComplete()
    }
}