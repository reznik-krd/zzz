package com.reznik.hotrit.models

import android.os.AsyncTask
import com.reznik.hotrit.domen.Expenses
import com.reznik.hotrit.models.dao.ExpensesDao

class ExpensesModel(val expensesDao: ExpensesDao) {

    fun loadExpenses(callback: LoadExpensesCallback) { LoadExpensesTask(callback).execute() }

    fun addExpense(expense: Expenses, callback: CompleteCallback) { AddExpenseTask(callback).execute(expense) }

    fun clearExpenses(completeCallback: CompleteCallback) { ClearExpensesTask(completeCallback).execute() }

    //_______________________________________________________________________________________________________________________

    interface LoadExpensesCallback { fun onLoad(expenses: List<Expenses>) }

    interface CompleteCallback { fun onComplete() }

    //_______________________________________________________________________________________________________________________

    inner class LoadExpensesTask(private val callback: LoadExpensesCallback) : AsyncTask<Void?, Void?, List<Expenses>>() {
        override fun doInBackground(vararg params: Void?) = expensesDao.all()
        override fun onPostExecute(expenses: List<Expenses>) = callback.onLoad(expenses)
    }

    inner class AddExpenseTask(private val callback: CompleteCallback) : AsyncTask<Expenses, Void?, Void?>() {
        override fun doInBackground(vararg params: Expenses): Void? {
            params[0].let { expensesDao.insert(it) }
            return null
        }
        override fun onPostExecute(aVoid: Void?) = callback.onComplete()
    }

    inner class ClearExpensesTask(private val callback: CompleteCallback) : AsyncTask<Void?, Void?, Void?>() {
        override fun doInBackground(vararg params: Void?): Void? {
            expensesDao.deleteAll()
            return null
        }
        override fun onPostExecute(aVoid: Void?) = callback.onComplete()
    }
}