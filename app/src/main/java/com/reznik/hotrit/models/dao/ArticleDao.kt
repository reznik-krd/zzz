package com.reznik.hotrit.models.dao

import androidx.room.*
import com.reznik.hotrit.domen.Article
import java.util.*

@Dao
abstract class ArticleDao {

    @Query("select * from article")
    abstract fun all(): List<Article>?

    @Query("select * from article where id = :id")
    abstract fun getById(id: Long): Article?

    @Query("select * from article where name = :name")
    abstract fun getByName(name: String): Article?

    @Query("select * from article where name like '%' || :search || '%'")
    abstract fun searchByName(search: String): List<Article>?

    fun insert(article: Article) {
        article.uuidRel = article.uuidRel ?: UUID.randomUUID()
        insertDao(article)
    }

    @Insert
    abstract fun insertDao(article: Article)

    @Update
    abstract fun update(article: Article)

    @Delete
    abstract fun delete(article: Article)

    @Query("delete from article")
    abstract fun deleteAll()
}