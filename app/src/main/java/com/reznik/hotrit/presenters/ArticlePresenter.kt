package com.reznik.hotrit.presenters

import com.reznik.hotrit.contracts.ArticleContractView
import com.reznik.hotrit.domen.Article
import com.reznik.hotrit.models.ArticleModel

class ArticlePresenter(
    private val model: ArticleModel,
    var view: ArticleContractView? = null
) {

    fun attachView(view: ArticleContractView) = run { this.view = view }

    fun detachView() = run { view = null }

    fun viewIsReady() = run { loadArticles() }

    fun searchEmpty() = run { loadArticles() }

    fun searchArticles() = run {
        view?.getSearchData()?.let {
            model.searchArticles(it, object : ArticleModel.LoadArticlesCallback {
                override fun onLoad(articles: List<Article>) {
                    view?.showArticles(articles)
                }
            })
        }
    }

    fun loadArticles() {
        model.loadArticles(object : ArticleModel.LoadArticlesCallback {
            override fun onLoad(articles: List<Article>) {
                view?.showArticles(articles)
            }
        })
    }

    fun add() {
        view?.enterArticleData()
    }

    fun delete(valueObj: Any) {
        model.articleDao.delete(valueObj as Article)
        loadArticles()
    }

    fun clear() {
        model.clearArticles(object : ArticleModel.CompleteCallback {
            override fun onComplete() {
                loadArticles()
            }
        })
    }
}