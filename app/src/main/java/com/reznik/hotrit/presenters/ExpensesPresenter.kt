package com.reznik.hotrit.presenters

import com.reznik.hotrit.contracts.ExpensesContractView
import com.reznik.hotrit.domen.Expenses
import com.reznik.hotrit.models.ExpensesModel

class ExpensesPresenter(
    private val model: ExpensesModel,
    var view: ExpensesContractView? = null
) {

    fun attachView(view: ExpensesContractView) = run { this.view = view }

    fun detachView() = run { view = null }

    fun viewIsReady() = run { loadExpenses() }

    fun loadExpenses() {
        model.loadExpenses(object : ExpensesModel.LoadExpensesCallback {
            override fun onLoad(expenses: List<Expenses>) {
                view?.showExpenses(expenses.sortedByDescending { it.dateCreate })
            }
        })
    }

    fun delete(valueObj: Any) {
        model.expensesDao.delete(valueObj as Expenses)
        loadExpenses()
    }

    fun clear() {
        model.clearExpenses(object : ExpensesModel.CompleteCallback {
            override fun onComplete() {
                loadExpenses()
            }
        })
    }
}