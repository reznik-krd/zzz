package com.reznik.hotrit.domen

import android.content.Context
import android.view.*
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.reznik.hotrit.R
import com.reznik.hotrit.models.dao.ArticleDao
import com.reznik.hotrit.utils.CONTEXT_MENU_ITEM_DELETE
import com.reznik.hotrit.utils.CONTEXT_MENU_ITEM_EDIT
import com.reznik.hotrit.activities.OnContextMenuItem
import java.text.DateFormat

class ExpensesAdapter(
    val context: Context,
    val articleDao: ArticleDao
) : RecyclerView.Adapter<ExpensesAdapter.ExpensesHolder>() {

    private var data: MutableList<Expenses> = ArrayList()
    var onContextMenuItem: OnContextMenuItem? = null

    fun setData(expenses: List<Expenses>) {
        data.clear()
        data.addAll(expenses)
        notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ExpensesHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.expenses_item,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ExpensesHolder, position: Int) = run {
        //TODO: отвратительно
        val expenses = data[position]
        val article = articleDao.getById(expenses.articleId!!)

        holder.nameShortEI?.text = DateFormat.getDateInstance(DateFormat.MEDIUM).format(expenses.dateCreate!!)
        holder.nameShortExtraEI?.text = "${expenses.amount} руб."
        holder.fullNameEI?.text = article?.name
        holder.fullNameExtraEI?.text = expenses.uuidRel.toString()
    }

    inner class ExpensesHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnCreateContextMenuListener {

        var nameShortEI: TextView? = null
        var nameShortExtraEI: TextView? = null
        var fullNameEI: TextView? = null
        var fullNameExtraEI: TextView? = null
        var cvEI: CardView? = null

        private val onSelectContextMenu: MenuItem.OnMenuItemClickListener = MenuItem.OnMenuItemClickListener {
            onContextMenuItem?.onClickItem(data[layoutPosition], it.itemId)
            true
        }

        init {
            nameShortEI = itemView.findViewById<View>(R.id.tvNameShortEI) as TextView
            nameShortExtraEI = itemView.findViewById<View>(R.id.tvNameShortExtraEI) as TextView
            fullNameEI = itemView.findViewById<View>(R.id.tvNameFullEI) as TextView
            fullNameExtraEI = itemView.findViewById<View>(R.id.tvNameFullExtraEI) as TextView
            cvEI = itemView.findViewById<View>(R.id.cvEI) as CardView

            itemView.setOnCreateContextMenuListener(this)
        }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
            menu?.run {
                add(Menu.NONE, CONTEXT_MENU_ITEM_EDIT, 1, R.string.edit).setOnMenuItemClickListener(onSelectContextMenu)
                add(Menu.NONE, CONTEXT_MENU_ITEM_DELETE, 2, R.string.delete).setOnMenuItemClickListener(onSelectContextMenu)
            }
        }
    }
}