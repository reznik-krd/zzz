package com.reznik.hotrit.domen

import android.content.Context
import android.widget.Toast
import androidx.room.*
import com.reznik.hotrit.utils.ExpensesGroupKey
import java.io.Serializable
import java.util.*

@Entity(
    tableName = "expenses",
    indices = [Index("article_id")],
    foreignKeys = [
        ForeignKey(
            onDelete = ForeignKey.CASCADE,
            entity = Article::class,
            parentColumns = ["id"],
            childColumns = ["article_id"]
        )]
)
class Expenses(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0,

    @ColumnInfo(name = "article_id")
    var articleId: Long? = null,

    @ColumnInfo(name = "date_create")
    var dateCreate: Date? = null,

    @ColumnInfo(name = "amount")
    var amount: Double? = 0.0,

    @ColumnInfo(name = "uuid_rel")
    var uuidRel: UUID? = null

) : Serializable {
    override fun toString() = "ID: $id article_id: $articleId amount: $amount date: $dateCreate"

    fun validate(context: Context) : Boolean {
        mutableListOf<String>().apply {
            articleId ?: add("Статья не указана")
            dateCreate ?: add("Дата не указана")
        }.takeIf { it.isNotEmpty() }?.let {
            Toast.makeText(context, it.joinToString(), Toast.LENGTH_SHORT).show()
            return false
        } ?: run {
            return true
        }
    }

    fun toKeyGroup() = ExpensesGroupKey(
        articleId = articleId
    )
}
