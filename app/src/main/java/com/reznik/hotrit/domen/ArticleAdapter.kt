package com.reznik.hotrit.domen

import android.content.Context
import android.view.*
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.reznik.hotrit.R
import com.reznik.hotrit.activities.OnClickItem
import com.reznik.hotrit.activities.OnContextMenuItem
import com.reznik.hotrit.utils.CONTEXT_MENU_ITEM_DELETE
import com.reznik.hotrit.utils.CONTEXT_MENU_ITEM_EDIT

class ArticleAdapter(val context: Context) : RecyclerView.Adapter<ArticleAdapter.ArticleHolder>() {

    private var data: MutableList<Article> = ArrayList()
    var onClickItem: OnClickItem? = null
    var onContextMenuItem: OnContextMenuItem? = null

    fun setData(articles: List<Article>) {
        data.clear()
        data.addAll(articles)
        notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ArticleHolder(LayoutInflater.from(parent.context).inflate(R.layout.article_item, parent, false))

    override fun onBindViewHolder(holder: ArticleHolder, position: Int) = run {
        holder.nameShortAI?.text = data[position].name
        holder.fullNameAI?.text = data[position].uuidRel.toString()
    }

    inner class ArticleHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnCreateContextMenuListener {

        var nameShortAI: TextView? = null
        var fullNameAI: TextView? = null
        var cvAI: CardView? = null

        private val onSelectContextMenu: MenuItem.OnMenuItemClickListener = MenuItem.OnMenuItemClickListener {
            onContextMenuItem?.onClickItem(data[layoutPosition], it.itemId)
            true
        }

        init {
            nameShortAI = itemView.findViewById<View>(R.id.tvNameShortAI) as TextView
            fullNameAI = itemView.findViewById<View>(R.id.tvNameFullAI) as TextView
            cvAI = itemView.findViewById<View>(R.id.cvAI) as CardView

            itemView.setOnClickListener { onClickItem?.onClick(data[layoutPosition]) }
            itemView.setOnCreateContextMenuListener(this)
        }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
            menu?.run {
                add(Menu.NONE, CONTEXT_MENU_ITEM_EDIT, 1, R.string.edit).setOnMenuItemClickListener(onSelectContextMenu)
                add(Menu.NONE, CONTEXT_MENU_ITEM_DELETE, 2, R.string.delete).setOnMenuItemClickListener(onSelectContextMenu)
            }
        }
    }
}