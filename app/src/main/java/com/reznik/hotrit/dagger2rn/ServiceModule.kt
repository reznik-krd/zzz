package com.reznik.hotrit.dagger2rn

import com.reznik.hotrit.models.AppDatabase
import com.reznik.hotrit.service.ExpensesService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ServiceModule {

    @Provides
    @Singleton
    fun provideExpensesService(appDatabase: AppDatabase): ExpensesService {
        return ExpensesService(appDatabase.expensesDao())
    }
}