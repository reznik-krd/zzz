package com.reznik.hotrit.dagger2rn

import android.content.Context
import androidx.room.Room
import com.reznik.hotrit.models.dao.ArticleDao
import com.reznik.hotrit.models.dao.ExpensesDao
import com.reznik.hotrit.models.AppDatabase
import com.reznik.hotrit.utils.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun provideArticleDao(appDatabase: AppDatabase): ArticleDao {
        return appDatabase.articleDao()
    }

    @Provides
    @Singleton
    fun provideExpensesDao(appDatabase: AppDatabase): ExpensesDao {
        return appDatabase.expensesDao()
    }

    @Provides
    @Singleton
    fun provideAppDataBase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME).allowMainThreadQueries().build()
    }
}