package com.reznik.hotrit.dagger2rn

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(
    var appContext: Context
) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return appContext
    }
}