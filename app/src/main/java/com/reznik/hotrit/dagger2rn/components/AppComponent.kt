package com.reznik.hotrit.dagger2rn.components

import com.reznik.hotrit.activities.*
import com.reznik.hotrit.dagger2rn.*
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, StorageModule::class, PresenterModule::class, AdapterModule::class, ServiceModule::class])
@Singleton
interface AppComponent {
    fun injectArticleActivity(articleActivity: ArticleActivity)
    fun injectArticleEnter(articleEnter: ArticleEnter)
    fun injectReportingActivity(reportingActivity: ReportingActivity)
    fun injectExpensesEnter(expensesEnter: ExpensesEnter)
    fun injectExpensesActivity(expensesActivity: ExpensesActivity)
    fun injectMainActivity(mainActivity: MainActivity)
}